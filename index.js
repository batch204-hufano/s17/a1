// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function yourDetails (){
		let yourfullName = prompt("Enter Your Full Name: ");
		let yourAge = prompt("Enter Your Age: ");
		let yourAddress = prompt ("Enter Your Address: ");

		alert("Thank you for entering your details:")

		console.log("Hello, " + yourfullName);
		console.log("You are " + yourAge + " years old.")
		console.log("You live in " + yourAddress);
	}
	yourDetails();
	




/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBandArtist (){
		console.log("1. Arthur Nery");
		console.log("2. Jush Hush");
		console.log("3. Adie Garcia");
		console.log("4. Zack Tabudlo");
		console.log("5. Ben&Ben");
	}

	favoriteBandArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovies (){
		console.log("1. Apocalypto")
		console.log("Rotten Tomatoes Rating: 97%")

		console.log("2. The Old Guard")
		console.log("Rotten Tomatoes Rating: 96%")

		console.log("3. Doctor Strange")
		console.log("Rotten Tomatoes Rating: 93%")

		console.log("4. Light Year")
		console.log("Rotten Tomatoes Rating: 91%")

		console.log("5. Annabelle Creation")
		console.log("Rotten Tomatoes Rating: 95%")
	}

	favoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);